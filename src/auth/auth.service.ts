import { Injectable } from "@nestjs/common";

@Injectable({})
export class AuthService {
    signin() {
        return { msg: "Hi from signed In" }
    }

    signup() {
        return { msg: "Hi from signed Up" }
    }
}